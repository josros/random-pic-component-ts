const template = document.createElement('template');
template.innerHTML = `
  <style>
    #img-container {
      display: inline-block;
      padding: 10px;
      background: grey;
    }
  </style>
  <div id="img-container"></div>
`

class RandomPic extends HTMLElement {  
  private static readonly defaultFreq: number = 5000;
  private static readonly defaultLength: number = 300;
  
  private element: any;
  private imageUrl: string;
  private image?: HTMLImageElement;
  private timeout: any;
  
  static get observedAttributes() {
    return [ 'update-freq-ms', 'image-length-px' ];
  }

  set updateFreqMs(value: number) {
    this.setAttribute("update-freq-ms", `${value}`);
  }

  get updateFreqMs(): number {
    const updateFreqAttr = this.getAttribute("update-freq-ms");
    if(updateFreqAttr) {
      return parseInt(updateFreqAttr);
    }
    return RandomPic.defaultFreq;
  }

  set imageLengthPx(value) {
    this.setAttribute("image-length-px", value);
  }

  get imageLengthPx() {
    return this.getAttribute("image-length-px")  || `${RandomPic.defaultLength}`
  }
  
  constructor() {
    super();
    const templateContent = template.content;

    this.attachShadow({mode: 'open'}).appendChild(
      templateContent.cloneNode(true)
    );

    this.imageUrl = `https://picsum.photos/${this.imageLengthPx}/${this.imageLengthPx}?`;
  }

  connectedCallback() {
    console.log('RandomPic added to DOM');
    if(this.shadowRoot) {
      this.element = this.shadowRoot.querySelector('div');
    
      this.image = document.createElement("img");
      this.image.src = this.imageUrl;
      this.element.appendChild(this.image);
      this.redefineTimeout(this.updateFreqMs)
    }
  }

  disconnectedCallback() {
    console.log('RandomPic removed from DOM');
    clearTimeout();
  }

  attributeChangedCallback(name: string, oldVal: any, newVal: any) {
    if (oldVal !== newVal) {
       console.log(`${name} changed from ${oldVal} to ${newVal}`)
       switch(name) {
        case 'update-freq-ms':
            this.updateFreqMsUpdated(newVal);
            break;
        case 'image-length-px':
            this.imageLengthPxUpdated(newVal);
            break;
        }
    }
  }

  updateFreqMsUpdated(newVal: string) {
    try {
      this.assertInteger(newVal);
      const asNumber = parseInt(newVal);
      this.updateFreqMs = asNumber;
      this.clearTimeout();
      this.redefineTimeout(this.updateFreqMs);
    } catch(err) {
      throw Error("update-freq-ms must be a number")
    }
    
  }

  imageLengthPxUpdated(newVal: string) {
    try {
      this.assertInteger(newVal);
      this.imageLengthPx = newVal;
      this.imageUrl = `https://picsum.photos/${this.imageLengthPx}/${this.imageLengthPx}?`;
    } catch(err) {
      throw Error("image-length-px must be a number")
    }
  }

  assertInteger(value: string) {
    parseInt(value) // should fail if nan
  }

  redefineTimeout(timeout: number) {
    this.timeout = setTimeout(() => {
      this.timeoutCallback(timeout);
    }, timeout);
  }

  timeoutCallback(timeout: number) {
    // date required to ignore cache
    if(this.image) {
      this.image.src = this.imageUrl + new Date().getTime()
      this.redefineTimeout(timeout);
    }
  }

  clearTimeout() {
    if(this.timeout) {
      clearTimeout(this.timeout);
    }
  }
}

const elementName = 'random-pic';
window.customElements.define(elementName, RandomPic);

export { elementName };