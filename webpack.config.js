const { ModuleFederationPlugin } = require("webpack").container;
const path = require("path");

let config = {
  entry: "./src/index.ts",
  devServer: {
    contentBase: __dirname,
    publicPath: '/',
    compress: true,
    port: 3003
  },
  output: {
    filename: '[name].js',
    path: path.join(__dirname, 'dist')
  },
  optimization: {
    minimize: false
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        options: {
          configFile: 'tsconfig.json'
        }
      }
    ]
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "randomPic",
      library: { type: "var", name: "randomPic" },
      filename: "randomPicEntry.js",
      exposes: {
        "./component": "./src/random-pic/random-pic.ts",
      }
    })
  ],
};

module.exports = (env, argv) => {
  if (argv.mode === 'development') {
    config.output.publicPath = 'http://localhost:3003/';
  }

  if (argv.mode === 'production') {
    config.output.publicPath = "https://josros.gitlab.io/random-pic-component-ts/"
  }

  return config;
};
